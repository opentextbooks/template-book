# Template Sub-chapter

**This is a template subchapter.** Use it to familiarise yourself with the structure of chapters, subchapters and headings. It also has an image, a "note", a "hint", a "warning" and a "dropdown" down below. 

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent at commodo nisi. Pellentesque erat eros, eleifend quis rutrum at, consectetur in lorem. Nulla tempus ligula nec eros tincidunt, id rhoncus nunc mattis. Duis vel ornare tortor. Etiam non justo mollis nisi lacinia fermentum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dignissim pharetra felis scelerisque ullamcorper.

## Heading 2
Nulla at aliquet ipsum. Maecenas urna ante, dapibus sit amet nulla vel, iaculis imperdiet massa. Quisque aliquam leo sed placerat porttitor. Donec varius in sapien eu maximus. In tempor dolor in placerat viverra. Praesent convallis purus velit, sit amet accumsan eros molestie a. Ut semper urna rutrum, sodales ex sit amet, maximus massa. Mauris vulputate sagittis quam nec congue. Donec hendrerit orci quis est sollicitudin, id tempor sapien mattis. Aliquam aliquam nisl ut volutpat sollicitudin. In sit amet lorem sem. Aliquam auctor venenatis mauris, et lacinia mauris posuere ut. Ut id lorem neque. Mauris arcu tellus, commodo vel tempor vitae, consequat in metus. Nam massa dui, convallis nec viverra sed, commodo eu dui.


```{figure} images/tudelft.png
:name: fig:TUDelft
TU Delft logo.
```

Maecenas ac dapibus velit. Vestibulum eu faucibus sapien. Proin a nibh eget velit sodales blandit. Praesent pellentesque mollis orci, at ornare justo ultricies quis. Integer scelerisque commodo mauris, eget interdum odio facilisis at. Praesent nisi tellus, tristique porttitor iaculis eu, feugiat sit amet elit. Praesent sed lacinia enim. In massa arcu, volutpat vel viverra vitae, varius sed lectus. Aliquam aliquam velit metus, in porttitor ex tincidunt sit amet. Quisque lobortis tellus at bibendum congue. Quisque at convallis enim. Fusce interdum tempor venenatis.

```{note}
This is a note
```

```{tip}
This is a tip
```
```{warning}
This is a warning
```

```{exercise} Dropdown
:class: dropdown
This is a dropdown box.
```


### Heading 3
Fusce imperdiet laoreet ornare. Sed interdum convallis risus, ac porttitor risus eleifend in. Etiam ut metus blandit, posuere magna eu, aliquam justo. Praesent interdum sit amet libero non pellentesque. Curabitur a maximus tellus, eu facilisis lorem. Sed efficitur magna pretium, finibus elit vel, sodales felis. Cras mollis nunc ac lectus luctus, auctor congue libero consequat. Donec sit amet orci ac purus vehicula faucibus. Ut hendrerit dui quis turpis dignissim gravida. Duis imperdiet sapien a tellus aliquet, id condimentum nulla pretium. Nam eleifend risus non tellus porttitor, sit amet ultrices quam finibus. Morbi faucibus id dolor non congue. Nulla elementum accumsan tortor nec tempor. Vestibulum tempor vulputate velit, at consequat leo pulvinar ac. Pellentesque quam nibh, porta nec nibh nec, efficitur convallis sapien. Etiam vel interdum nisl.
