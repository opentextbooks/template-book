# Open textbooks with Jupyter books

Welcome to the Jupyter Book template for TU Delft! This template is designed to make it easy for lecturers to create their own custom textbooks, lecture notes, or other educational materials for their courses.

With Jupyter Book, you can write your content using Markdown, Jupyter Notebooks, etc., and easily organize it into chapters and sections. You can also add interactive elements like code snippets, visualizations, and quizzes to engage your students and enhance their learning experience.

Whether you're creating a textbook from scratch or adapting existing materials, this template provides a solid starting point and customization options to fit your needs. For a more extensive example of the possiblities, see the demo book made by [Timon Idema](https://textbooks.open.tudelft.nl/textbooks/catalog/book/73). 

This collection of examples is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
